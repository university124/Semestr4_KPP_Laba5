package client;

import interfaces.Result;
import java.io.*;
import java.net.Socket;

public class Client {
    private static final String HOST = "localhost";
    private static final int PORT = 12345;

    public static void main(String[] args) {
        try (Socket socket = new Socket(HOST, PORT)) {
            System.out.println("Connected to the server");

            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

            // Отправка class-файла задачи на сервер
            String classFile = "out/production/Laba_5/client/JobOne.class";
            System.out.println("Sending class file: " + classFile);
            out.writeObject(classFile);
            try (FileInputStream fis = new FileInputStream(classFile)) {
                byte[] classData = new byte[fis.available()];
                fis.read(classData);
                out.writeObject(classData);
            }
            System.out.println("Class file sent to server");

            // Отправка объекта задачи на сервер
            int num = 20; // Например, вычисляем факториал 20
            JobOne job = new JobOne(num);
            out.writeObject(job);
            System.out.println("Job sent to server");

            // Получение результата от сервера
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            classFile = (String) in.readObject();
            System.out.println("Received result class file name: " + classFile);
            byte[] resultClassData = (byte[]) in.readObject();
            try (FileOutputStream fos = new FileOutputStream(classFile)) {
                fos.write(resultClassData);
            }
            System.out.println("Result class file saved as: " + classFile);

            // Получение и десериализация объекта результата
            Result result = (Result) in.readObject();
            System.out.println("Result received and deserialized");
            System.out.println("Result = " + result.output() + ", Time taken = " + result.scoreTime() + " ns");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
