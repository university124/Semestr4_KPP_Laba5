package server;

import interfaces.Executable;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static final int PORT = 12345;

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is listening on port " + PORT);

            while (true) {
                try (Socket socket = serverSocket.accept()) {
                    System.out.println("New client connected");

                    ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

                    // Получение и сохранение class-файла задачи
                    String classFile = (String) in.readObject();
                    System.out.println("Received class file name: " + classFile);
                    classFile = classFile.replaceFirst("client", "server");
                    byte[] classData = (byte[]) in.readObject();
                    try (FileOutputStream fos = new FileOutputStream(classFile)) {
                        fos.write(classData);
                    }
                    System.out.println("Class file saved as: " + classFile);

                    // Получение объекта задачи и выполнение задачи
                    Executable task = (Executable) in.readObject();
                    System.out.println("Task received and deserialized. Starting execution...");
                    double startTime = System.nanoTime();
                    Object result = task.execute();
                    double endTime = System.nanoTime();
                    double timeTaken = endTime - startTime;
                    System.out.println("Task executed. Time taken: " + timeTaken + " ns");

                    // Создание объекта результата
                    ResultImpl resultObject = new ResultImpl(result, timeTaken);
                    ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

                    // Отправка class-файла и результата обратно клиенту
                    out.writeObject(classFile);
                    try (FileInputStream fis = new FileInputStream(classFile)) {
                        byte[] resultClassData = new byte[fis.available()];
                        fis.read(resultClassData);
                        out.writeObject(resultClassData);
                    }
                    out.writeObject(resultObject);
                    System.out.println("Result sent back to client.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
